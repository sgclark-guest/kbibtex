/***************************************************************************
*   Copyright (C) 2004-2012 by Thomas Fischer                             *
*   fischer@unix-ag.uni-kl.de                                             *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/
#ifndef BIBTEXFILEEXPORTERTOOLCHAIN_H
#define BIBTEXFILEEXPORTERTOOLCHAIN_H

#include <QProcess>

#include <KTempDir>

#include "fileexporter.h"

class QString;
class QStringList;

/**
@author Thomas Fischer
*/
class KBIBTEXIO_EXPORT FileExporterToolchain : public FileExporter
{
    Q_OBJECT
public:
    static const QString keyBabelLanguage;
    static const QString defaultBabelLanguage;

    static const QString keyBibliographyStyle;
    static const QString defaultBibliographyStyle;

    FileExporterToolchain();

    virtual void reloadConfig() = 0;

    static bool kpsewhich(const QString &filename);
    static bool which(const QString &filename);

public slots:
    void cancel();

protected:
    KTempDir tempDir;

    bool runProcesses(const QStringList &progs, QStringList *errorLog = NULL);
    bool runProcess(const QString &cmd, const QStringList &args, QStringList *errorLog = NULL);
    bool writeFileToIODevice(const QString &filename, QIODevice *device, QStringList *errorLog = NULL);

private:
    QProcess *m_process;
    QStringList *m_errorLog;

private slots:
    void slotReadProcessStandardOutput();
    void slotReadProcessErrorOutput();

};

#endif
